/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      		/* -b  option; if 0, dmenu appears at bottom */
static int instant = 0;					/* -n  option; if 1, when there is only one possible selection remaining, it will automatically be chosen */
static int indent = 0;					/* -i  option: if 1, indents items in a vertical menu (normally default, but this build is different)*/
static int centered = 0;                    		/* -c  option; if 1, will center dmenu on the screen */
static int one_return = 0;				/* -o  option; if 1, disables shift-return & ctrl-return + works with mouse patch (for multiselect, mainly)*/
static int plainprompt = 0;                		/* -p  option; if 1, prompt uses SchemeNorm, instead of SchemeSel */
static int min_width = 500;                 		/* minimum width when centered */

/* -fn option overrides fonts[0]; default X11 font or font set */
static char *fonts[] = {
	"monospace:size=11",
};
static char *prompt      = NULL;      /* -p  option; prompt to the left of input field */

static const unsigned int opaque = 0xFFU;

static unsigned int alphas[][3]      = {
		/*               fg      bg        border     */
	[SchemeNorm] = 		{ opaque, 208, opaque },
	[SchemeSel] = 		{ opaque, 208, opaque },
	[SchemeOut] = 		{ opaque, 208, opaque },
	[SchemeHp] = 		{ opaque, 208, opaque },
};

static char *colors[SchemeLast][2] = {
				/*     fg         bg       */
	[SchemeNorm] = 		{ "#bbbbbb", "#222222" },
	[SchemeSel] = 		{ "#eeeeee", "#005577" },
	[SchemeOut] = 		{ "#000000", "#00ffff" },
	[SchemeHp] = 		{ "#bbbbbb", "#ff0000" },
};

/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 0;

/* -H option; minimum height of a menu line */
static unsigned int lineheight = 0;
static unsigned int min_lineheight = 8;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static unsigned int border_width = 0;
